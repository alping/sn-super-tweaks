# Super Tweaks - Standard Notes Extension

**Super Tweaks is a layerable theme that provides:**

- Changes the font preferences
- Limiting the width of the editor in a better way than with Editor Width
